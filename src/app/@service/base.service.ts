import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(
    public http: HttpClient
  ) { }

  login(req): any {
    return this.http.get(`http://localhost:3000/users?username=${req.name}&password=${req.password}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  getUserList(id): any {
    return this.http.get(`http://localhost:3000/leaves?user_id=${id}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }

  getUsersList(): any {
    return this.http.get(`http://localhost:3000/leaves`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  getUserLists(): any {
    return this.http.get(`http://localhost:3000/users?roleId=2`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }

  getAllUsers(req): Observable<any> {
    console.log(req);
    return this.http.get(`http://localhost:3000/users?roleId=${req}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  saveNewUser(req: any): any {
    return this.http.post(`http://localhost:3000/users`, req).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  saveUser(req: any): any {
    return this.http.post('http://localhost:3000/leaves', req).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  updateLeave(req) {
    return this.http.put(`http://localhost:3000/leaves/${req.id}`, req).pipe(
      map(res => res),
      catchError(err => err)
    )
  }

  updateStatus(req) {
    return this.http.put(`http://localhost:3000/leaves/${req.id}`, req).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  getFilterUser(req): any {
    return this.http.get(`http://localhost:3000/users?username=${req}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  getFilterLeaves(req): any {
    return this.http.get(`http://localhost:3000/leaves?${req.name ? 'name=' + req.name : ''}&formDate<${req.date}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  getsingleUser(req: any): any {
    return this.http.get(`http://localhost:3000/leaves?id=${req.id}&user_id=${req.user_id}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  deletesingleuser(req: any): any {
    console.log('service response for delete', req);
    return this.http.delete(`http://localhost:3000/leaves/${req.id}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  deleteSingleuser(req: any): Observable<any> {
    return this.http.delete(`http://localhost:3000/users/${req.id}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }

  updateUser(req: any): any {
    return this.http.put(`http://localhost:3000/users/${req.id}`, req).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
  getSingleUser(req: any): any {
    return this.http.get(`http://localhost:3000/users?id=${req.id}`).pipe(
      map(res => res),
      catchError(err => err)
    )
  }
}
