import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserlayoutRoutingModule } from './userlayout-routing.module';
import { UserlayoutComponent } from './userlayout.component';
import { UserheaderModule } from 'src/app/@components/user/userheader/userheader/userheader.module';


@NgModule({
  declarations: [UserlayoutComponent],
  imports: [
    CommonModule,
    UserlayoutRoutingModule,
    UserheaderModule 
  ]
})
export class UserlayoutModule { }
