import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminlayoutRoutingModule } from './adminlayout-routing.module';
import { AdminlayoutComponent } from './adminlayout.component';
import { AdminheaderModule } from 'src/app/@components/admin/adminheader/adminheader/adminheader.module';


@NgModule({
  declarations: [AdminlayoutComponent],
  imports: [
    CommonModule,
    AdminlayoutRoutingModule,
    AdminheaderModule 
  ]
})
export class AdminlayoutModule { }
