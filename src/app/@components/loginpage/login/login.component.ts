import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { BaseService } from 'src/app/@service/base.service';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd';
import { trigger, state, style, transition, keyframes, animate } from '@angular/animations';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger("sildeIn", [
      state("in", style({ transform: "translateX(1px)" })),
      state("out", style({ transform: "translateX(100px" })),
      transition("in <=> out", animate('1000ms ease-in'))
    ]),
    trigger("slideDown", [
      state("up", style({ transform: "translateX(1px)" })),
      state("down", style({ transform: "translateY(700px)" })),
      state("erase", style({ transform: "translateY(1500px)" })),
      transition("up <=> down", animate('1000ms ease-in')),
      transition("down<=> erase", animate('1000ms ease-in'))
    ])
  ]
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public forgotForm: FormGroup;
  // public notification: NzNotificationService
  expression: any = 'in';
  forgotanimate: any = 'up';
  forget1:any=true;
  public user_id:any;
  constructor(
    public fb: FormBuilder,
    public baseService: BaseService,
    public router: Router,
    public notification: NzNotificationService
  ) {
    this.createNewForm();
    this.forgotForm = this.fb.group({
      email: [null,Validators.compose([Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")])],
    })

  }

  ngOnInit() {
  }
  createNewForm() {
    this.loginForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    })
  }


  loginClick(type: string) {
    if (this.loginForm.valid) {
      let req = {
        name: this.loginForm.controls.username.value,
        password: this.loginForm.controls.password.value
      }
      this.baseService.login(req).subscribe(loginResponse => {
        console.log('success')
        console.log('loginResponse', loginResponse);
        // console.log(loginResponse[0].id)

        if (loginResponse.length > 0) {
          this.expression = this.expression == "out" ? "in" : "out";
          this.notification.success(
            'Notification',
            'Login Successfull'
          );
          this.expression = this.expression == "out" ? "in" : "out";
          localStorage.setItem('token', JSON.stringify(loginResponse && loginResponse[0]))
          console.log(JSON.parse(localStorage.getItem('token')))
          let a = JSON.parse(localStorage.getItem('token'));
          console.log(a.roleId);
          this.user_id=a.id;
            debugger;
          if (a.roleId == 1) {
            debugger;
            this.expression = this.expression == "out" ? "in" : "out";
            this.router.navigate(['/adminlayout']);
          } else if (a.roleId == 2) {
            this.expression = this.expression == "out" ? "in" : "out";
            this.router.navigate(['/userlayout']);
          } else {
            this.notification.create(
              type,
              'Notification',
              'Username or Password is incorrect'
            );
            this.router.navigate(['/login']);
          }
          // }else{
          //   this.notification.create(
          //     type,
          //     'Notification',
          //     'Username or Password is incorrect'
          //   );
          // }
        } else {
          this.notification.error(
            'Notification', 
            'Username or Password is incorrect'
          );
        }
        console.log("this.loginForm.value");



      })

    } else {
      this.validateAllFormFields(this.loginForm);
    }

  }


  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  forget() {
    this.forgotanimate = this.forgotanimate == "up" ? "down" : "up";
  }
  backClicked(){
    this.forgotanimate="up";
  }
  sendEmail(){
    if(this.forgotForm.valid){
      this.notification.success(
        'Notification',
        'Email sent successfully!!'
      );
      console.log("Email has been sent");
      this.forgotanimate="up";
      this.forgotForm.reset();
      // this.forget1=false;
    }else{
      this.validateAllFormFields(this.forgotForm);
    }
  }

}
