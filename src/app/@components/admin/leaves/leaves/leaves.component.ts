import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseService } from 'src/app/@service/base.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
declare var $: any

@Component({
  selector: 'app-leaves',
  templateUrl: './leaves.component.html',
  styleUrls: ['./leaves.component.scss']
})
export class LeavesComponent implements OnInit {
  public userForm: FormGroup;
  public filterForm: FormGroup;
  p:number=1;
  constructor(
    public baseService: BaseService,
    public fb: FormBuilder
  ) {
    this.createForm();
    this.createFilterForm();
  }
 
  userData: any[] = [];
  createForm() {
    this.userForm = this.fb.group({
      id: [null, Validators.required],
      reason: null,
      fromDate: null,
      toDate: null,
      status: null,
      name: null,
      user_id: null,
      roleId: null
    })
  }

  createFilterForm() {
    this.filterForm = this.fb.group({
      filterName: null,
      date:null
    })
  }

  getSingleUser(data) {
    console.log('data', data);
    let req = {
      id: data.id,
      user_id: data.user_id
    }
    this.baseService.getsingleUser(req).subscribe(getUserByIdREsponse => {
      console.log({ getUserByIdREsponse });
      let res = getUserByIdREsponse[0];
      this.userForm.patchValue({
        reason: res && res.reason || null,
        fromDate: res.fromDate,
        toDate: res.toDate,
        id: res.id,
        status: res.status,
        name: res.name,
        user_id: res.user_id,
        roleId: res.roleId
      });
      this.openModal();
    },
      err => console.log(err));
  }


  openModal() {
    $('#modal').modal('show');
  }


  closeModal() {
    $('#modal').modal('hide');
    // this.userForm.reset();
  }
  getUserList() {
    this.baseService.getUsersList().subscribe(res => {
      console.log('res', res);
      this.userData = res || [];
    })
  }
  ngOnInit() {
    this.getUserList();
  }
  getUsersByName(req) {
    this.userData = req || [];
  }
  filterClick() {
      let req = {
        name: this.filterForm.controls.filterName.value,
        date: this.filterForm.controls.date.value
      }
      console.log(req);
      this.baseService.getFilterLeaves(req).subscribe(res => {
        console.log('filter response', res);
        this.getUsersByName(res);
      })
  }

  clearFilters() {
    this.getUserList();
    this.filterForm.controls.filterName.setValue(null);
    this.filterForm.controls.date.setValue(null);
  }

  approval(req) {
    console.log(req);
    req.patchValue({
      status: "Approved"
    })
    this.baseService.updateStatus(req.value).subscribe(res => {
      console.log('response after service', res);
      this.getUserList();
      this.closeModal();
    })

  }
  decline(req) {
    console.log(req);
    req.patchValue({
      status: "Declined"
    })
    this.baseService.updateStatus(req.value).subscribe(res => {
      console.log('response after service', res);
      this.getUserList();
      this.closeModal();
    })
  }




}
