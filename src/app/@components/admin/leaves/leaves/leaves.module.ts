import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { NzTableModule } from 'ng-zorro-antd/table';
// import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';


import { LeavesRoutingModule } from './leaves-routing.module';
import { LeavesComponent } from './leaves.component';

 
@NgModule({
  declarations: [LeavesComponent],
  imports: [
    CommonModule,
    LeavesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NzTableModule,
    NgxPaginationModule  ]
})
export class LeavesModule { }
