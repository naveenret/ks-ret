import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { BaseService } from 'src/app/@service/base.service';
declare var $: any;
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public filterForm: FormGroup;
  usersData: any[] = [];
  public userForm: any;
  p:number=1;
  constructor(
    public fb: FormBuilder,
    public baseService: BaseService
  ) {
    this.createNewFilterForm();
    this.createNewUserForm();
  }

  createNewFilterForm() {
    this.filterForm = this.fb.group({
      filterName: [null, Validators.required]
    })
  }

  ngOnInit() {
    console.log(this.userForm.value);
    this.getUsers(this.userForm.controls.roleId.value);
  }



  createNewUserForm() {
    this.userForm = this.fb.group({
      id: [null],
      username: [null, Validators.required],
      password: [null, Validators.required], 
      // user_id: null,
      email: [null,Validators.compose([Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")])],
      roleId: "2"
    })
  }

  getUsers(roleId) {
    this.baseService.getAllUsers(roleId).subscribe(res => {
      console.log('response after the service', res);
      this.usersData = res || [];
    })
  }


  newUser() {
    this.openModal();
 
  }
  openModal() {
    $('#modal').modal('show');
  }

  closeModal() {
    this.userForm.reset();
    $('#modal').modal('hide');

  }
  saveUser() {

    if(this.userForm.valid){
      console.log(this.userForm.value);
      if (this.userForm.controls.id.value) {
        this.baseService.updateUser(this.userForm.value).subscribe(res=>{
          console.log(res);
          this.baseService. getUserLists().subscribe(res=>{
            console.log(res); 
            this.usersData=res || [];
            this.closeModal();
          })
        })
  
      } else {
          this.baseService.saveNewUser(this.userForm.value).subscribe(res => {
            console.log('user response', res);
            let a = res.roleId;
            this.getUsers(a);
          })
          this.closeModal();
        
      }
    }else{
      this.validateAllFormFields(this.userForm);
    }
    
  }
  
  getUsersByName(req) {
    this.usersData = req || [];
  }
  filterClick() {
    let a = this.filterForm.controls.filterName.value;
    this.baseService.getFilterUser(a).subscribe(res => {
      console.log(res);
      this.getUsersByName(res);
    })

  }
  getUserList() {
    this.baseService.getUserLists().subscribe(res => {
      console.log('res', res);
      this.usersData = res || [];
    })
  }
  clearFilters() {
    this.getUserList();
    this.filterForm.controls.filterName.setValue(null);
  }
  getSingleUser(user) {
    this.baseService.getSingleUser(user).subscribe(res => {
      console.log(res);
      this.userForm.patchValue({
        id: res[0].id,
        username: res[0].username,
        password: res[0].password,
        // user_id: res[0].user_id,
        email: res[0].email,
        roleId: res[0].roleId
      })
      this.openModal();
    })
  }
  updateUser(user) {
    console.log('respone before passing the update user', user);
    this.userForm.patchvalue(
      {
        id: user[0].id,
        username: user[0].username,
        password: user[0].password,
        // user_id: user[0].user_id,
        email: user[0].email,
        roleId: "2"
      }
    )
    this.baseService.updateUser(user).subscribe(res => {
      console.log('response for getting the user', res)
    })
  }
  deleteUser(user) {
    console.log('delete response before calling service',user);
    return this.baseService.deleteSingleuser(user).subscribe(res=>{
      console.log('user deleting response',res);
      this.baseService. getUserLists().subscribe(res=>{
        console.log(res);
        this.usersData=res || [];
      })
    })
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
