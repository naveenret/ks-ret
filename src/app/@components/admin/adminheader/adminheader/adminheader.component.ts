import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, transition, keyframes, animate } from '@angular/animations';


@Component({
  selector: 'app-adminheader',
  templateUrl: './adminheader.component.html',
  styleUrls: ['./adminheader.component.scss'],
  animations: [
    trigger("sildeOut", [
      state("in", style({ transform: "translateX(1px)" })),
      state("out", style({ transform: "translateX(100px" })),
      transition("in <=> out", animate('1000ms ease-in'))
    ])
  ]
})
export class AdminheaderComponent implements OnInit {
  public token: any;
  public user: any;
  public expression: any = false;
  expression1: any = 'in';

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
    let a = JSON.parse(localStorage.getItem('token'));
    this.token = a.username;
  }
  imgClick() {
    this.expression = this.expression == false ? true : false;
  }

  logout() {
    this.expression1 = "out"; 
    setTimeout(() => {
      this.router.navigate(['/login']);
    }, 500)
    localStorage.clear();
  }
  leaves() {
    this.router.navigate(['/adminlayout/leaves']);
  }
  users() {
    this.router.navigate(['/adminlayout/users']);
  }
}
