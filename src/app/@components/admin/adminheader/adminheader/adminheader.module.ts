import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminheaderRoutingModule } from './adminheader-routing.module';
import { AdminheaderComponent } from './adminheader.component';


@NgModule({
  declarations: [AdminheaderComponent],
  imports: [
    CommonModule,
    AdminheaderRoutingModule
  ],
  exports:[AdminheaderComponent]
})
export class AdminheaderModule { }
