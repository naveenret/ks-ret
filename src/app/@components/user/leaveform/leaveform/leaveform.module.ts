import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NgxPaginationModule } from 'ngx-pagination';

import { LeaveformRoutingModule } from './leaveform-routing.module';
import { LeaveformComponent } from './leaveform.component';


@NgModule({
  declarations: [LeaveformComponent],
  imports: [
    CommonModule,
    LeaveformRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NzTableModule,
    NgxPaginationModule 
  ]
})
export class LeaveformModule { }
