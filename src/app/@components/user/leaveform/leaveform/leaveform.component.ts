import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { BaseService } from 'src/app/@service/base.service';
import { IfStmt } from '@angular/compiler';
declare var $: any

@Component({
  selector: 'app-leaveform',
  templateUrl: './leaveform.component.html',
  styleUrls: ['./leaveform.component.scss']
})
export class LeaveformComponent implements OnInit {
  public userForm: any;
  public id: any;
  public currentUser: any;
  p:number=1;
  constructor(  
    public fb: FormBuilder,
    public baseService: BaseService
  ) {
    this.createNewFormModal();

  }
  userData: any[] = [];
  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('token'))
    console.log('currentUser', this.currentUser);
    this.id = this.currentUser.id;
    this.getUserData(this.id);
  }
  createNewFormModal() {
    this.userForm = this.fb.group({
      id: [null],
      reason: [null, Validators.required],
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required],
      status: "Pending",
      name: null,
      user_id: this.id,
      roleId: null
    })
  }
  getUserData(id) {
    this.baseService.getUserList(id).subscribe(res => {
      console.log('res', res);
      this.userData = res || [];
    })
  }
  openModal() {
    $('#modal').modal('show');
  }
  closeModal() {
    this.userForm.reset()
    $('#modal').modal('hide');
    let a = JSON.parse(localStorage.getItem('token'))
    this.getUserData(a && a.id);

  }

  getSingleUser(data) {
    console.log('data', data);
    console.log(data.status);
    if (data.status == "Pending") {
      let req = {
        id: data.id,
        user_id: data.user_id
      }
      this.baseService.getsingleUser(req).subscribe(res => {
        console.log('res', res);
        this.userForm.patchValue({
          id: res[0].id,
          user_id: res[0].user_id,
          roleId: res[0].roleId,
          reason: res[0].reason,
          fromDate: res[0].fromDate,
          toDate: res[0].toDate,
          status: res[0].status
        });
        console.log(this.userForm)
        console.log(this.userForm.value)
        this.openModal();
      })
    }
  }

  deleteSingleUser(data) {
    if (data.status == "Pending") {
      let req = {
        id: data.id,
        user_id: data.user_id
      }
      this.baseService.deletesingleuser(req).subscribe(res => {
        console.log('response after deleting', res);
        this.getUserData(this.currentUser.id);
      })
    }

  }

  saveModal(req) {
    if(this.userForm.valid){

      console.log(req)
      if (this.userForm.controls.id.value) {
        console.log('req', req);
        this.baseService.updateLeave(this.userForm.value).subscribe(res => {
          console.log('res', res);
          this.closeModal();
  
        })
      } else{
          this.userForm.patchValue({
            status: "Pending",
            name: this.currentUser.username,
            user_id: this.currentUser.id,
            roleId: this.currentUser.roleId
          })
          console.log('userForm', this.userForm)
          this.baseService.saveUser(this.userForm.value).subscribe(res => {
            console.log('res', res); 
            this.getUserData(res.user_id);
          })
          this.closeModal();
      }
    }else{
      this.validateAllFormFields(this.userForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
