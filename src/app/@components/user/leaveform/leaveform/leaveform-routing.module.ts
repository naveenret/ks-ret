import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveformComponent } from './leaveform.component';


const routes: Routes = [
  {
    path:'',
    component:LeaveformComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeaveformRoutingModule { }
