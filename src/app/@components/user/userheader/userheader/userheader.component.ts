import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger,state,style,transition,keyframes,animate } from '@angular/animations';


@Component({
  selector: 'app-userheader',
  templateUrl: './userheader.component.html',
  styleUrls: ['./userheader.component.scss'],
  animations: [
    trigger("sildeOut",[
      state("in",style({transform : "translateX(1px)"})),
      state("out",style({transform:"translateX(100px"})),
      transition("in <=> out",animate('1000ms ease-in'))
    ]) 
  ]
})
export class UserheaderComponent implements OnInit {
public expression:any=false;
expression1:any="in";
public token:any;
  constructor(
    public router:Router
  ) { }

  ngOnInit() {
    let a=JSON.parse(localStorage.getItem('token'))
    this.token=a.username;
  }
  imgClick(){
    this.expression=this.expression==false?true:false;

  }
  logout(){
    this.expression1="out";
    setTimeout(() => {
      this.router.navigate(['/login']);
    }, 500)
    localStorage.clear();
  }

}
