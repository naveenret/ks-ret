import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './@guard/auth.guard';
import { AdminlayoutComponent } from './@layouts/adminlayout/adminlayout/adminlayout.component';
import { UserlayoutComponent } from './@layouts/userlayout/userlayout/userlayout.component';


const routes: Routes = [
  {
    path:'',
    redirectTo:'login',
    pathMatch:'full'
  },
  {
    path:'login',
    loadChildren:'./@components/loginpage/login/login.module#LoginModule'
  },
  
  {
    path:'adminlayout',
    component:AdminlayoutComponent,
    // loadChildren:'./@layouts/adminlayout/adminlayout/adminlayout.module#AdminlayoutModule',
    canActivate:[AuthGuard],
    children:[
      {
        path:'',
        redirectTo:'leaves',
        pathMatch:'full'
      },
      {
        path:'leaves',
        loadChildren:'./@components/admin/leaves/leaves/leaves.module#LeavesModule'
      },
      {
        path:'users',
        loadChildren:'./@components/admin/user/user/user.module#UserModule'
      }
 
    ]
  },
  {
    path:'userlayout',
    component:UserlayoutComponent,
    // loadChildren:'./@layouts/adminlayout/adminlayout/adminlayout.module#AdminlayoutModule',
    canActivate:[AuthGuard],
    children:[
      {
        path:'',
        redirectTo:'leaveform',
        pathMatch:'full'
      },
      {
        path:'leaveform',
        loadChildren:'./@components/user/leaveform/leaveform/leaveform.module#LeaveformModule'
      }
 
    ]
  },
  {
    path:'**',
    redirectTo:'login',
    pathMatch:'full'
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
